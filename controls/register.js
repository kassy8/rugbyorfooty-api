const handleRegister= (req, res, pgdb, bcrypt, saltRounds) => {
    const {name,email,password}= req.body;
    const hash= bcrypt.hashSync(password,saltRounds);
    pgdb.transaction(trx => {
        trx.insert({
            hash: hash,
            email: email,
            last_login: new Date()
        })
        .into('user_logins')
        .returning('email')
        .then(loginEmail => {
            return trx('users')
            .returning('*')
            .insert({
                name: name,
                email: loginEmail[0],
                password: hash,
                joined: new Date()
            })
            .then(user => {
                res.status(200).json(user[0]);
            });
        })
        .then(trx.commit)
        .catch(trx.rollback)
    })
    .catch(err => res.status(400).json(err));
};

module.exports= {
    handleRegister: handleRegister
}