
const handleProfile= (req, res, pgdb)=>{
    const {id} = req.params;
    pgdb.select('*').from('users').where('id',id)
    .then(user => {
        if(user.length){
            res.status(200).json(user[0]);
        }else{
            res.status(400).json('no user found');
        }
    })
    .catch(err => res.status(400).json(err));
}

module.exports= {
    handleProfile: handleProfile
}