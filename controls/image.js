
const handleImage= (req, res, pgdb)=>{
    const {userid,phototypeid} = req.body;
    let update_column= "unknown";
    switch(phototypeid){
        case "fb": update_column= "football"; break;
        case "rl": update_column= "rugbyl"; break;
        case "ru": update_column= "rugbyu"; break;
        case "uk": update_column= "unknown"; break;
        default: break;
    }
    pgdb.from('users').where('id',userid)
    .increment(update_column,1)
    .returning(update_column)
    .then(totals => {
        if(totals.length){
            res.status(200).json(totals[0]);
        }else{
            res.status(400).json('couldn\'t update '+update_column);
        }
    })
    .catch(err => res.status(400).json(err));
};

module.exports= {
    handleImage: handleImage
}