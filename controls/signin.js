const handleSignin= (req, res, pgdb, bcrypt)=>{
    const {email,password}= req.body;
    pgdb.select('email','hash').from('user_logins')
    .where('email',email)
    .then(data => {
        const isValid = bcrypt.compareSync(password,data[0].hash);
        if(isValid){
            return pgdb.select('*').from('users')
                .where('email',email)
                .then(user => {
                    res.status(200).json(user[0]);
                })
                .catch(err => res.status(400).json(err)); //unable to get user
        }else{
            res.status(400).json('Wrong credetials'); 
        }
    })
    .catch(err => res.status(400).json(err)); //unable to get user
}

module.exports= {
    handleSignin: handleSignin
}