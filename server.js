//const DB = require('./DB.js');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const bcrypt = require('bcrypt');
const saltRounds = 5;

const knex = require('knex');

const Register = require('./controls/register');
const Signin = require('./controls/signin');
const Profile = require('./controls/profile');
const Image = require('./controls/image');

const app= express();
app.use(bodyParser.json());
app.use(cors());


const pgdb= (process.env.DATABASE_URL === undefined) ? 
    knex({
        client: 'pg',
        connection: {
            host: '127.0.0.1',
            user: 'postgres',
            password: 'test',
            database: 'rugbyorfooty'
        }
    })
: 
    knex({
        client: 'pg',
        connection: {
            connectionString: process.env.DATABASE_URL,
            ssl: true,
        }
    }) 
;


//using a GET method on ROOT / retuning the request and a response in function
app.get("/", (req,res)=>{res.status(200).json('Server ready...')});

/*
DESCRIPTION: -----
Take a users email and password and return a user object if credetials match someone
*/
app.post("/signin",(req, res) => Signin.handleSignin(req, res, pgdb, bcrypt));

/*
DESCRIPTION: -----
Take name, email, password and create a new user
*/
app.post("/register",(req, res) => Register.handleRegister(req, res, pgdb, bcrypt, saltRounds));

/* 
DESCRIPTION: -----
Take user's id and show the user object
*/
app.get("/profile/:id",(req, res) => Profile.handleProfile(req, res, pgdb));

/*
DESCRIPTION -----
Take user's id and show the user object
*/
app.put("/image",(req, res) => Image.handleImage(req, res, pgdb));


app.listen(process.env.PORT || 3003, ()=>{
    console.log(`server running from port # ${process.env.PORT} or 3003?`);
});